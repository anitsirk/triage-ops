# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/team_label_inference'

RSpec.describe Triage::TeamLabelInference do
  include_context 'with event', 'Triage::IssuableEvent' do
    let(:object_kind)            { 'issue' }
    let(:action)                 { 'open' }
    let(:from_gitlab_org)        { true }
    let(:gitlab_bot_event_actor) { false }
    let(:label_names)            { ['group::dynamic analysis'] }

    let(:event_attrs) do
      {
        object_kind: object_kind,
        action: action,
        from_gitlab_org?: from_gitlab_org,
        gitlab_bot_event_actor?: gitlab_bot_event_actor
      }
    end

    let(:comment_body_with_label_quick_action) do
      <<~MARKDOWN.chomp
        #{label_quick_action}
      MARKDOWN
    end

    let(:label_quick_action) { '/label ~"devops::plan" ~"section::dev"' }
  end

  shared_examples 'nothing happens' do
    it 'nothing happens' do
      expect_no_request do
        subject.process
      end
    end
  end

  subject { described_class.new(event) }

  before do
    stub_request(:get, "https://about.gitlab.com/sections.json")
      .to_return(status: 200, body: read_fixture('sections.json'), headers: {})

    stub_request(:get, "https://about.gitlab.com/stages.json")
      .to_return(status: 200, body: read_fixture('stages.json'), headers: {})

    stub_request(:get, "https://about.gitlab.com/groups.json")
      .to_return(status: 200, body: read_fixture('groups.json'), headers: {})
  end

  include_examples 'registers listeners', %w[issue.open issue.reopen issue.update merge_request.open merge_request.reopen merge_request.update]

  describe '#applicable?' do
    include_examples 'applicable on contextual event'

    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when event was authored by gitlab bot' do
      let(:gitlab_bot_event_actor) { true }

      include_examples 'event is not applicable'
    end

    context 'with Section label' do
      let(:label_names) { %w[section::dev] }

      include_examples 'event is applicable'
    end

    context 'with Stage label' do
      let(:label_names) { %w[devops::create] }

      include_examples 'event is applicable'
    end

    context 'with Group label' do
      let(:label_names) { ['group::source code'] }

      include_examples 'event is applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    context 'when no Group, Stage or Section is present' do
      let(:action) { 'update' }
      let(:label_names) { [] }

      it_behaves_like 'nothing happens'
    end

    context 'when only Group is present' do
      context 'with supported group label' do
        let(:label_names) { ['group::dynamic analysis'] }
        let(:label_quick_action) { '/label ~"section::sec" ~"devops::secure"' }

        it 'infers Stage and Section labels', :aggregate_failures do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end

      context 'with unsupported group label' do
        let(:label_names) { ['group::memory'] }

        it_behaves_like 'nothing happens'
      end
    end

    context 'when only Stage is present' do
      let(:action) { 'reopen' }
      let(:label_names) { %w[devops::create] }
      let(:label_quick_action) { '/label ~"section::dev"' }

      context 'with supported Stage label' do
        it 'infers Section label' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end

      context 'with unsupported Stage label' do
        let(:label_names) { %w[devops::enablement] }

        it_behaves_like 'nothing happens'
      end
    end

    context 'when only Section is present' do
      let(:label_names) { %w[section::ops] }

      it_behaves_like 'nothing happens'
    end

    context 'when only Stage and Group are present' do
      let(:object_kind) { 'merge_request' }
      let(:action) { 'update' }

      context 'when both group and Stage labels are unsupported' do
        let(:label_names) { %w[devops::enablement group::verify] }

        it_behaves_like 'nothing happens'
      end

      context 'with unsupported group label' do
        let(:label_names) { %w[devops::create group::verify] }
        let(:label_quick_action) { '/label ~"section::dev"' }

        it 'only infers the Section label from the supported Stage label' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end

      context 'with unsupported Stage label' do
        let(:label_names) { %w[devops::enablement group::editor] }
        let(:label_quick_action) { '/label ~"section::dev" ~"devops::create"' }

        it 'infers both stage and section labels from the group label' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end

      context 'when the Stage and Group match' do
        let(:label_names) { %w[devops::create group::editor] }
        let(:label_quick_action) { '/label ~"section::dev"' }

        it 'only infers the Section label' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end

      context 'when the Stage and Group do not match' do
        let(:object_kind) { 'merge_request' }
        let(:action) { 'reopen' }
        let(:label_names) { %w[devops::configure group::editor] }
        let(:label_quick_action) { '/label ~"section::dev" ~"devops::create"' }

        it 'infers the Stage from the Group and infers the Section from the Group' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end
    end

    context 'when Section, Stage and Group and all are matching' do
      let(:label_names) { ['section::sec', 'devops::secure', 'group::dynamic analysis'] }

      it_behaves_like 'nothing happens'
    end

    context 'when Section, Stage and Group are present, but Stage label is unsupported' do
      let(:label_names) { ['section::sec', 'devops::enablement', 'group::dynamic analysis'] }
      let(:label_quick_action) { '/label ~"devops::secure"' }

      it 'sets the appropriate Stage label to align with Section and Group' do
        expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
          subject.process
        end
      end
    end

    context 'when Section, Stage and Group are present but Stage is mismatched from Section and Group' do
      let(:label_names) { ['section::sec', 'devops::create', 'group::dynamic analysis'] }
      let(:label_quick_action) { '/label ~"devops::secure"' }

      it 'sets the appropriate Stage label to align with Section and Group' do
        expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
          subject.process
        end
      end
    end

    context 'when Section, Stage and Group are present but Group is mismatching with Section and Stage' do
      let(:label_names) { ['section::ops', 'devops::create', 'group::project management'] }
      let(:label_quick_action) { '/label ~"devops::plan" ~"section::dev"' }

      it 'updates the Stage and Section to match the Group' do
        expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
          subject.process
        end
      end
    end

    context 'when Section, Stage and Group are present but Group label is unsupported' do
      context 'when Stage and Section labels match' do
        let(:label_names) { ['section::dev', 'devops::create', 'group::memory'] }

        it_behaves_like 'nothing happens'
      end

      context 'when Stage and Section labels do not match' do
        let(:label_names) { ['section::ops', 'devops::create', 'group::memory'] }
        let(:label_quick_action) { '/label ~"section::dev"' }

        it 'infers section label from stage' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end
    end

    context 'when only Section and Group are present' do
      context 'with supported group label' do
        let(:label_names) { ['section::sec', 'group::dynamic analysis'] }
        let(:label_quick_action) { '/label ~"devops::secure"' }

        it 'infers the Stage from Group' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end

      context 'with unsupported group label' do
        let(:label_names) { ['section::sec', 'group::memory'] }

        it_behaves_like 'nothing happens'
      end
    end

    context 'when only Section and Stage are present' do
      context 'when they are matching' do
        let(:label_names) { %w[section::sec devops::secure] }

        it_behaves_like 'nothing happens'
      end

      context 'when they are mismatching' do
        context 'with unsupported Stage label' do
          let(:label_names) { %w[devops::enablement] }

          it_behaves_like 'nothing happens'
        end

        context 'with supported Stage label' do
          let(:label_names) { %w[section::dev devops::secure] }
          let(:label_quick_action) { '/label ~"section::sec"' }

          it 'the Stage takes precedence and updates the Section' do
            expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
              subject.process
            end
          end
        end
      end
    end
  end

  describe '#validated_group_name' do
    context 'with label group::distribution and Distribution:Deploy"' do
      let(:label_names) { %w[group::distribution Distribution:Deploy] }

      it 'returns distribution_deploy' do
        expect(subject.send(:validated_group_name)).to eq('distribution_deploy')
      end
    end

    context 'with label group::distribution and no subgroup name"' do
      let(:label_names) { %w[group::distribution] }

      it 'returns distribution_build as default' do
        expect(subject.send(:validated_group_name)).to eq('distribution_build')
      end
    end

    context 'with label group::application performance' do
      let(:label_names) { ['group::application performance'] }

      it 'returns application_performance' do
        expect(subject.send(:validated_group_name)).to eq('application_performance')
      end
    end

    context 'with unsupported group name' do
      let(:label_names) { ['group::memory'] }

      it 'returns nil' do
        expect(subject.send(:validated_group_name)).to be_nil
      end
    end

    context 'with invalid group name' do
      let(:label_names) { ['group::does not exist'] }

      it 'raises error' do
        expect { subject.send(:validated_group_name) }.to raise_error(
          ArgumentError,
          'does_not_exist group is not found in groups api response.'
        )
      end
    end
  end

  describe '#validated_stage_name_from_devops_label' do
    context 'with label devops::secore' do
      let(:label_names) { ['devops::secure'] }

      it 'returns secure' do
        expect(subject.send(:validated_stage_name_from_devops_label)).to eq('secure')
      end
    end

    context 'with label devops::anti-abuse' do
      let(:label_names) { ['devops::anti-abuse'] }

      it 'returns secure' do
        expect(subject.send(:validated_stage_name_from_devops_label)).to eq('anti-abuse')
      end
    end

    context 'with unsupported Stage label' do
      let(:label_names) { ['devops::enablement'] }

      it 'returns nil' do
        expect(subject.send(:validated_stage_name_from_devops_label)).to be_nil
      end
    end

    context 'with invalid Stage label' do
      let(:label_names) { ['devops::does not exist'] }

      it 'raises error' do
        expect { subject.send(:validated_stage_name_from_devops_label) }.to raise_error(
          ArgumentError,
          'does not exist stage is not found in stages api response.'
        )
      end
    end
  end
end
